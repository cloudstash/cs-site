---
title: About Me
date: 2021-07-01 16:18:10
---

Hi.  My name is Nick Newbill. As a full fledge nerd, however you'd like to term it, I have developed over 15 years of experience managing server infrastructures and data-center operations across multiple platforms.   My experience continues with both advisory and hands-on SAS implementations. I've been working with SAS Software for the last 8 years, first with a partner consulting company, then directly for SAS after 3 years of service. My alignment at SAS is with Private Sector Fraud and Compliance under US Professional Services where I provide value, maybe :), in helping to keep multiple environments in a functional state.  Technical advice or maddening issues always occur and having someone like me in tow allows everyone else on the project team to stay focused on their specific tasks.
Before I began my journey with SAS Software I was just a tech junkie working either in a company IT department or in other consulting based roles.  Regardless of what or where, my focus has always revolved inside the technical realm along with an overarching component: support.  In many ways, I am just an expert problem searcher first and problem solver second.  Working under the hood in the platform layer is always close to home and where I tend to excel the most.
